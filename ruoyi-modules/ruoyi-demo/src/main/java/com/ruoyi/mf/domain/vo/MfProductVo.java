package com.ruoyi.mf.domain.vo;

import com.ruoyi.mf.domain.MfProduct;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.excel.convert.ExcelDictConvert;
import io.github.linpeilie.annotations.AutoMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
import com.ruoyi.common.orm.core.domain.TreeEntity;

/**
 * 产品树视图对象 mf_product
 *
 * @author 数据小王子
 * @date 2024-01-06
 */
@Data
@ExcelIgnoreUnannotated
@EqualsAndHashCode(callSuper = true)
@AutoMapper(target = MfProduct.class)
public class MfProductVo extends TreeEntity implements Serializable
{

    @Serial
    private static final long serialVersionUID = 1L;

     /** 产品id */
    @ExcelProperty(value = "产品id")
    private Long productId;

     /** 产品名称 */
    @ExcelProperty(value = "产品名称")
    private String productName;

     /** 产品状态（0正常 1停用） */
    @ExcelProperty(value = "产品状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_student_status")
    private String status;

     /** 逻辑删除标志（0代表存在 1代表删除） */
    @ExcelProperty(value = "逻辑删除标志（0代表存在 1代表删除）")
    private Integer delFlag;

}
