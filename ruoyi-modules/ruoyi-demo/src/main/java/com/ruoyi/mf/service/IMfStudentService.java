package com.ruoyi.mf.service;

import java.util.List;
import com.ruoyi.mf.domain.MfStudent;
import com.ruoyi.mf.domain.vo.MfStudentVo;
import com.ruoyi.mf.domain.bo.MfStudentBo;
import com.ruoyi.common.orm.core.service.IBaseService;
import com.ruoyi.common.orm.core.page.TableDataInfo;

/**
 * 学生信息表Service接口
 *
 * @author 数据小王子
 * 2024-01-05
 */
public interface IMfStudentService extends IBaseService<MfStudent>
{
    /**
     * 查询学生信息表
     *
     * @param studentId 学生信息表主键
     * @return 学生信息表
     */
    MfStudentVo selectById(Long studentId);

    /**
     * 查询学生信息表列表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 学生信息表集合
     */
    List<MfStudentVo> selectList(MfStudentBo mfStudentBo);

    /**
     * 分页查询学生信息表列表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 分页学生信息表集合
     */
    TableDataInfo<MfStudentVo> selectPage(MfStudentBo mfStudentBo);

    /**
     * 新增学生信息表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 结果:true 操作成功，false 操作失败
     */
    boolean insert(MfStudentBo mfStudentBo);

    /**
     * 修改学生信息表
     *
     * @param mfStudentBo 学生信息表Bo
     * @return 结果:true 更新成功，false 更新失败
     */
    boolean update(MfStudentBo mfStudentBo);

    /**
     * 批量删除学生信息表
     *
     * @param studentIds 需要删除的学生信息表主键集合
     * @return 结果:true 删除成功，false 删除失败
     */
    boolean deleteByIds(Long[] studentIds);

}
